+++
title = "How to force backfill an entire Ceph pool"
+++
Perhaps you want to backfill a pool before the others, perhaps one pool is smaller than the others.

Anyway, if you want to force backfill a pool (in the example pool number 4), here's the command:
```
ceph pg force-backfill `ceph pg dump | cut -d' ' -f 1 | grep ^4\\. `
```
