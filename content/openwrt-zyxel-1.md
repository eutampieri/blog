+++
title = "Porting OpenWrt to ZyXEL NWA3560-N"
+++
A few days ago I got this device for free and I thought that maybe I could run OpenWrt on it, so that I could use it alongside with my other WPA3 OpenWrt hotspots.

A quick search for OpenWrt support came up empty but [wikidevi has many interesting informations about this device](https://wikidevi.wi-cat.ru/ZyXEL_NWA3560-N):

- it has the same CPU as the EdgeRouter lite
- it has two ath9k radios

## Time to open the device!

Here's what’s inside this access point:

![device PCB upside](https://forum.openwrt.org/uploads/default/original/3X/1/f/1f529fe466a065e39b2a30b3b452d573662684b5.jpeg)
![device PCB bottom](https://forum.openwrt.org/uploads/default/original/3X/2/a/2a1e5095f6692daf95b17845a7566b7beac34dcf.jpeg)

On a closer look you may have noticed that there's a smaller daughter board, on the left of the heatsink. It’s a 256 MB USB DOM (Disk on Module) manufactured by A-DATA, and that’s another similarity with the ER-Lite.

![DoM pinout](/immagini/USB_dom.png)
<center>_The disk on module pinout can be found at page 8 of [this PDF](https://www.cactus-tech.com/wp-content/uploads/2019/03/CT_PM_uDOM_100-2.0.pdf)_</center>

Dumping the DoM however reveals a different situation than the [EdgeRouter](https://openwrt.org/toh/ubiquiti/edgerouter.lite), that has a 4MB onboard flash that contains a bootloader that boots from the DoM plus a 4GB disk on module that contains the kernel and the rootfs, because there’s no kernel partition and the two partitions appear to contain only configuration data and some binary images that I wasn’t (`binwalk` wasn't I should say) able to analyse.

## What can I do now?

After poking around a bit with ZySh (the built in shell) and having found no way to get a regular shell (none of [this](https://th0mas.nl/2020/03/26/getting-root-on-a-zyxel-vmg8825-t50-router/) worked, even if maybe my hopes were a little too high), I’ve decided to have a look at the official firmware upgrade files.

They’re a ZIP archive containing different binary files, but more interestingly [this PDF](https://mega.nz/file/TsxwSZwK#dq2f48TrtmNO9bqrIFE7lsLt7cY1L2zzxpwkA6gltCg), where it’s stated how to interact with the bootloader.

_Note: there’s a root account which maybe has a true shell, but I didn’t know the password (and there was none in the configuration files) so I couldn’t use it. If you happen to find out what the password is, you can `ssh root@zyxel` and probably find some useful informations._

## Looking for a serial port

The board has a JTAG header (JP4) and an RS232 serial port that’s accessible via a mini DIN 6 connector located on the left of the RJ45 jack. The pinout is the following:

![Serial pinout, pin 2 is TX, pin 6 is RX and pin 3 is GND](https://forum.openwrt.org/uploads/default/original/3X/2/f/2f040dc63c72e25e1b1b35114b473b3917b89bc1.jpeg)

This piece of information was gathered by probing between the [RS232 to TTL chip](https://mega.nz/file/CtoURKaC#ge_Y1MlemGqOnFn4FYEpo3pkbuIKbNOiKme9y_hlc8I) and the serial port.

## What's next?

My journey ends here today, but this project isn’t finished yet and I hope to be able to boot OpenWrt on this AP soon.

The next thing to do is to figure out how to interact with the bootloader and [make the device boot a ramdisk image, as Borromini said](https://forum.openwrt.org/t/port-openwrt-to-zyxel-nwa3560-n/88287/5?u=eutampieri)

In the meantime I want to thank Borromini from the OpenWrt forum for pointing me in the right direction and you for having read this article.
