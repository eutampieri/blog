+++
title="Usare il VoIP di TIM con Asterisk"
+++
Inserisco qui la mia configurazione funzionante per usare TIM come trunk SIP per Asterisk.

La versione di Asterisk che sto usando è `Asterisk 16.2.1~dfsg-1+deb10u1` su debian 10 buster.

### Requisiti
* Credenziali VoIP reperibili attraverso l'assistenza tecnica TIM (io ho usato Angie, il loro chatbot)
* Dopo aver aperto la segnalazione, vi arriverà un SMS
* Cliccate il link e, dopo un po' di tempo, vi apparirà l'elenco delle vostre linee con i parametri

### Configurazione Asterisk
Andremo a modificare il file `sip.conf`. Sono necessarie due parti: una in alto (`register`) e una in basso (il peer vero e proprio)
```
srvlookup=yes                   ; Enable DNS SRV lookups on outbound calls
qualify=yes
register => NUM_TEL@telecomitalia.it:PASSWORD@OUTBOUND_PROXY:5060/NUM_TEL
[telecom]
	username=NUM_TEL
	type=friend
	context=provider
	secret=PASSWORD
	nat=force_rport,comedia
	disallow=all
	allow=ulaw,alaw
	host=telecomitalia.it
	insecure=invite
	outboundproxy=OUTBOUND_PROXY
	fromdomain=telecomitalia.it
	fromuser=NUMTEL
```
In pratica `PASSWORD` è la password della linea, come da configurazione fornita da TIM, `OUTBOUND_PROXY` si riferisce all'omonimo parametro fornito da TIM (`dxxxsx.co.imsw.telecomitalia.it`) e `NUM_TEL` è il numero di telefono senza spazi con +39 davanti.

## Modifica del 18/01/2021

Per evitare che le chiamate in arrivo termino dopo tre minuti (o meglio, l'audio in uscita termini), è possibile effettuare una configurazione come [qui](https://www.ilpuntotecnico.com/forum/index.php?topic=80169.0) riportato.

In pratica, si tratta di aggiungere una voce negli utenti SIP di Asterisk.

```
[telecom]
	username=+39xxx
	type=friend
	context=provider
	secret=x
	nat=force_rport,comedia
	disallow=all
	allow=ulaw,alaw
	host=telecomitalia.it
	insecure=invite
	outboundproxy=dxxxsx.co.imsw.telecomitalia.it
	fromdomain=telecomitalia.it
	fromuser=+39x
[timinbound]
	type=peer
	username=+39x
	secret=x
	qualify=no
	insecure=invite
	host=dxxxsx.co.imsw.telecomitalia.it
	fromuser=+39x
	fromdomain=telecomitalia.it
	realm=telecomitalia.it
```
