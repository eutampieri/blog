+++
title = "IBM Plex Mono on iOS"
+++
This is how I installed the font, using only Safari and the [iSH app](https://ish.app).

1. Download IBM Plex Mono from GitHub: `wget https://github.com/IBM/plex/releases/download/v6.4.0/IBM-Plex-Mono.zip`
1. Unzip the archive: `unzip IBM-Plex-Mono.zip`
1. Install dependencies: `apk add git npm`
1. Download the mobileconfig generator: `git clone https://github.com/jimmymasaru/fp-gen && cd fp-gen`
1. Generate the configuration profile: `node fpgen ../IBM-Plex-Mono/fonts/complete/ttf -o ../IBMPlexMono.mobileconfig -n "IBM Plex Mono"`
1. Mount your iOS filesystem and move the configuration profile to it: `mount -t ios . /mnt && mv ../IBMPlexMono.mobileconfig /mnt` (you have to chose an iOS folder)
1. Close iSH and open the generated file from Files.app
1. Open Settings.app and install the profile

If you trust me and don't want to follow these steps, you can download the profile from [here](/IBMPlexMono.mobileconfig).
