+++
title = "NodeRED Single Sign On with Keycloak"
+++
This morning I was struggling to get NodeRED to accept users from Keycloak. This is what acomplished my goals.

## Preliminary steps

You have to `npm install strategy-openidconnect`.

## Configuration

You have to configure the `adminAuth` section inside your `settings.js`:

```js
    adminAuth: {
        type:"strategy",
        strategy: {
            name: "openidconnect",
            label: 'Log in using Keycloak',
            icon:"fa-lock",
            strategy: require("passport-openidconnect").Strategy,
            options: {
                issuer: "https://keycloak.example.com/realms/myrealm",
                authorizationURL: "https://keycloak.example.com/realms/myrealm/protocol/openid-connect/auth",
                tokenURL: "https://keycloak.example.com/realms/myrealm/protocol/openid-connect/token",
                userInfoURL: "https://keycloak.example.com/realms/myrealm/protocol/openid-connect/userinfo",
                clientID: "nodered",
                clientSecret: "<yourClientSecret>",
                callbackURL: "http://192.168.60.102:1880/auth/strategy/callback",
                verify: function(token, tokenSecret, profile, done) {
                    done(null, {username: tokenSecret.username});
                }
            },
        },
        users: function(user) {
            let username = user;
            return new Promise(function(resolve) {
               if(username === undefined){
                   resolve(null);
               }
               var user = { username: username, permissions: ["*"] };
               resolve(user);
            });
       }
    },
```

There are two things worth noticing:
1. We call done with `tokenSecret` instead of with `profile`. I guess that by adding an `issuer` parameter at the beginning of the parameters it will be fixed.
1. We have to bind the username to a local variable before returning the promise. This is how JS works. but I'd forgotten.

