+++
title = "Sim IOT"
+++

Per l’utilizzo di SIM a scopo IOT credo che le offerte più vantaggiose
siano:

-   **Consumo inferiore ai 19,5 MB**: CoopVoce, piano tariffario
    *SuperFacile*. <span>0,10<span>€</span>/MB</span>

-   **Consumo superiore ai 19,5 MB ma inferiore ai 100 MB**: Fastweb,
    offerta *Mobile 100*. <span>100 MB, 1,95<span>€</span>/4
    settimane</span>

-   **Consumo superiore ai 100 MB ma inferiore ai 514 MB**: Tiscali,
    offerta *My Mobile Open*. <span>60 minuti, 512MB a
    3,00<span>€</span>/4 settimane</span>

-   **Consumo superiore ai 514 MB**: CoopVoce, offerta *Web 1 Giga senza
    limiti*. <span>1 GB, traffico oltre soglia gratis a 32 Kbps,
    4,00<span>€</span>/mese, circa 3,69<span>€</span>/4 settimane</span>


