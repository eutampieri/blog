+++
title = "Lucidare i soldi"
+++

Come far tornare le monete lucide
================================

*Eugenio Tampieri, 06/03/2017*

## Occorrente
* Monete da lucidare
* Alcool etilico (per rimuovere i grassi)
* Disinfettante (non necessario)
* Acido debole (acido acetico o citrico, per rimuovere gli ossidi)
* Acetone (solvente per rimuovere le eventuali impurità)

## Procedimento
 1. Preparare un bagno con gli ingredienti sovracitati
 2. Immergere le monete
 3. Aspettare 10 minuti
 4. Ripetere la procedura se necessario
 5. Risciacquare
 6. Le monete saranno lucidate

## Immagini

![](/immagini/IMG_7829.JPG)

Prima del trattamento

![](/immagini/IMG_7830.JPG)

Durante il trattamento

![](/immagini/IMG_7832.JPG)

Dopo il trattamento