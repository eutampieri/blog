+++
title = "Paperless ASN labels in NodeRED"
+++
If you want to generate ASN labels from NodeRED, you can use this workflow:

1. Get the next ASN from the Paperless API
1. Generate a sheet using the [paperless-asn-qr-code](https://github.com/entropia/paperless-asn-qr-codes)
1. Print it

More specifically, for step 2 I've built a Docker image: `registry.gitlab.com/eutampieri/containers/paperless-asn-generator`. It has a preconfigured environment variable (`LABEL_SHEET_FORMAT`) for the `--format` flag of the tool.

Finally, using NodeRED, you can use this flow:
```json
[
    {
        "id": "6e2739ed9919084d",
        "type": "tab",
        "label": "Flow 5",
        "disabled": false,
        "info": "",
        "env": []
    },
    {
        "id": "e46986cda4b7c263",
        "type": "inject",
        "z": "6e2739ed9919084d",
        "name": "",
        "props": [
            {
                "p": "payload"
            },
            {
                "p": "topic",
                "vt": "str"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "x": 180,
        "y": 160,
        "wires": [
            [
                "d058bd3fae46acbd"
            ]
        ]
    },
    {
        "id": "d058bd3fae46acbd",
        "type": "http request",
        "z": "6e2739ed9919084d",
        "name": "",
        "method": "GET",
        "ret": "txt",
        "paytoqs": "ignore",
        "url": "http://paperless-endpoint:8000/api/documents/next_asn/",
        "tls": "",
        "persist": false,
        "proxy": "",
        "insecureHTTPParser": false,
        "authType": "basic",
        "senderr": false,
        "headers": [],
        "x": 350,
        "y": 160,
        "wires": [
            [
                "9cfe04a37760b173"
            ]
        ]
    },
    {
        "id": "9cfe04a37760b173",
        "type": "change",
        "z": "6e2739ed9919084d",
        "name": "",
        "rules": [
            {
                "t": "set",
                "p": "url",
                "pt": "msg",
                "to": "\"http://label-container-endpoint/sheet?\"&$.payload",
                "tot": "jsonata"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 530,
        "y": 160,
        "wires": [
            [
                "0edb3b29da747c5c"
            ]
        ]
    },
    {
        "id": "0edb3b29da747c5c",
        "type": "http request",
        "z": "6e2739ed9919084d",
        "name": "",
        "method": "GET",
        "ret": "txt",
        "paytoqs": false,
        "url": "",
        "persist": false,
        "insecureHTTPParser": false,
        "authType": "",
        "senderr": false,
        "headers": [],
        "x": 710,
        "y": 160,
        "wires": [
            [
                "193ed5df9b5d39cc"
            ]
        ]
    },
    {
        "id": "193ed5df9b5d39cc",
        "type": "delay",
        "z": "6e2739ed9919084d",
        "name": "",
        "pauseType": "delay",
        "timeout": "30",
        "timeoutUnits": "seconds",
        "rate": "1",
        "nbRateUnits": "1",
        "rateUnits": "second",
        "randomFirst": "1",
        "randomLast": "5",
        "randomUnits": "seconds",
        "drop": false,
        "allowrate": false,
        "outputs": 1,
        "x": 180,
        "y": 240,
        "wires": [
            [
                "18e581d70d041515"
            ]
        ]
    },
    {
        "id": "18e581d70d041515",
        "type": "tcp request",
        "z": "6e2739ed9919084d",
        "name": "",
        "server": "printer.local",
        "port": "9100",
        "out": "time",
        "ret": "buffer",
        "splitc": "0",
        "newline": "",
        "trim": false,
        "tls": "",
        "x": 430,
        "y": 240,
        "wires": [
            []
        ]
    }
]
```
Remeber to set the correct credentials and the correct Docker container endpoint.

Moreover, I'm printing using JetDirect there, so modify that as you see fit.

