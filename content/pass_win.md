+++
title="ZX2C4's pass in Windows"
+++
Recently I've migrated to `pass`, but on [its website](https://passwordstore.org) there is no Windows installation guide. One of the things that helped me writing this guide is [this blog post](https://www.going-flying.com/blog/getting-zx2c4s-pass-and-passff-running-on-windows-10.html).

### Prerequisites
- [`git`](https://git-scm.com/download/win)
- If you want to integrate with Firefox, [Python 3](https://python.org) (If you're running Windows 10 and you don't have Python installed yet you just need to open the command prompt and type `python` followed by Enter. The Microsoft store will open and from there you can download Python)
- [GPG4Win](https://www.gpg4win.org/). No, the gpg bundled with git doesn't work.

### How do we put this together?
1. Import the GPG keys into Kleopatra (GPG keystore for Windows)
1. Make sure you can access the repo with git and that **you don't have incompatible path names** for Windows in it.
1. Download [gopass](https://www.gopass.pw/). From there you will download a zip file. You only need the exe file inside it.
1. Check your `PATH` by typing `PATH` inside `cmd`
    1. If there's a folder inside your user folder in it, place the `gopass.exe` in it and call it `pass.exe`.
    1. Otherwise, create a `bin` folder and add it to the `PATH` by:
        - Creating a `bin` folder somewere
        - In `explorer.exe`, right click This Computer > Properties
        - Click Advanced system settings (or similar)
        - Click Environment variables
        - Under User variables, select `Path`, then click Edit
        - Click Add, then type the path to the `bin` folder
        - Click OK twice
        - Logout and log back in
        - Place the downloaded `gopass.exe` in the `bin` folder and name it `pass.exe`
1. Inside your prompt/shell, type `pass clone repo_URL`
1. Now, if yoy type `pass ls` you should see all your passwords
1. You should have a working `pass` installation. While `gopass` is almost a drop-in replacement for `pass`, not all commands work the same. For example, `pass show` in `gopass` requires a website.

### Install a GUI
1. Download [QtPass](https://qtpass.org)
1. Right click the msi file and run it as administrator.
1. If you're lucky, the program will open. If it doesn't, open `regedit.exe` and go to `Computer\HKEY_CURRENT_USER\SOFTWARE\IJHack\QtPass`. Check that the variables reference correct executables and the password store.
![regedit screen with the right variables](/immagini/pass_regedit.png)
1. At this point, you should have a working GUI.

### Integrate with Firefox
1. Go to [this repo](https://github.com/passff/passff-host) and install the host application.
1. Go to `Computer\HKEY_CURRENT_USER\SOFTWARE\Mozilla\NativeMessagingHosts\passff` in `regedit.exe` and open the specified path. In my case the path is `%appdata%\passff`.
1. The `passff.py` file is made to work with the standard pass executable, so it will fail miserably in our case.
1. I've edited line 63 and replaced `show` with `ls`. [Here](/code/passff.py) is my modified version.
1. Now you can install [the extension](https://addons.mozilla.org/firefox/addon/passff) and, after restarting Firefox, your passwords should work.
1. There's an option in <a href="about:addons">about:addons</a> that autofills your passwords that you can enable by opening passff's options. ![passff option window](/immagini/passff.png)

If you find an issue or you want to comment the post you can head to its [GitLab page](https://gitlab.com/eutampieri/blog).
