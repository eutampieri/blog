+++
title = "Is Oracle agent resource intensive?"
+++

Just a quick note: on a small server (`Linux 5.4.17-2136.331.7.el8uek.x86_64 #3 SMP Mon May 6 15:17:51 PDT 2024 x86_64 x86_64 x86_64 GNU/Linux`, with roughly 600 MB of RAM) with Oracle Linux, disabling the agent (`sudo systemctl disable --now oracle-cloud-agent.service`) made a real difference in performance.
