+++
title = "Hi from a PowerMac G4 (or how I got git to run there)"
+++
I recently got a PowerMac G4 QuickSilver and I wanted to do some stuff on it.

![PowerMac G4](https://upload.wikimedia.org/wikipedia/commons/b/bb/Apple_PowerMac_G4_M8493_QuickSilver_front.jpg)
*baku13, CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons*

However, I wanted `git` to get some developement done on this machine. So I installed Xcode from the Tiger DVD, downloaded and copied on the PowerMac the current snapshot of git and ran make.

It didn't work.

After an evening of trial and errors (lots, in facts), I've finally come up with this invocation:
```
make install CFLAGS="-std=c99" NO_GETTEXT=YesPlease NO_CURL=YesPlease OPENSSL_SHA1=Yes
```

Then, after putting `export PATH=~/bin:~/.bin:$PATH` in `~/.profile` and running `git` I was greeted with this:

![git works](/immagini/git_g4.png)

## `make` parameters explaination

Let's break down the `make` invocation:
- `CFLAGS="-std=c99"` enables C99 in the installed GCC
- `NO_GETTEXT=YesPlease` Disable localisation support (maybe it is possible to use it but I am not sure, also YesPlease is so cute...)
- `NO_CURL=YesPlease` this means we cannot clone from HTTP(S) repos, but our `libcurl` is too old I guess. In the future I'd like to get this to work
- `OPENSSL_SHA1=Yes` This is funny... without this `git` builds but the SHA1 calculation is wrong and so clones fail (I suppose that every other operation does too, from how `git` works)
And that's it. However, you cannot just clone upstream and run the `make` above. I had to make a few modifications in the `Makefile` too, that you can see from the [commits](https://gitlab.com/modernppc/git/-/commits/master) `181244914df3e0a28250201a6ced71c56e56d5f8` and `bb143585b760e920b87d3a92a06a33d0f48a7e64`.

Those deal with missing symbols from linked libraries.

And with that I'm done for a while! Bye from my G4!
![Greetings from TextWrangler](/immagini/greetings_tw.png)